const csv = require("fast-csv")
const moment = require("moment")
const fs = require("fs")
const readLastLine = require("read-last-lines")

/**
 * Return an array representing the fields
 * of the last row in a csv file
 * @param {*string} path the path of the csv file
 * @param {*string} delimiter the fields separator
 * @param {*string} comment a carachter representing a comment line (if present)
 */
const getLastRow = async path => {
  const stringLines = await readLastLine.read(path, 2)
  const arrayLines = stringLines.split("\n")
  // The last line might be empty. In that case return the previous one
  const stringLastRow = arrayLines[1] === "" ? arrayLines[0] : arrayLines[1]
  const arrayLastRow = stringLastRow.split(/\ +/)
  return arrayLastRow
}

/**
 * Read the first 6 columns of the row parsed
 * from the VIMonitor file
 * and returns a string of the date
 * @param {*string} row
 */
const parseDateTimeFromRow = row => {
  const dateString = row.slice(0, 6).join(" ")
  const date = moment(dateString, "YYYY MM DD HH mm ss").toISOString()
  return date
}

/**
 * Get the the modified time property from
 * a feil
 * @param {*string} path the file path
 */
const getUpdatedFileDate = path => {
  return new Promise((res, rej) => {
    fs.stat(path, (err, stats) => {
      if (err) {
        rej(err)
      }
      res(stats.mtime)
    })
  })
}

/**
 * Return the Voltage and Current parameters in
 * a JSON formatted object, stripping away
 * the timestamp and useless columns.
 * @param {*array} row an array representing the fields
 * of a line of the csv file
 */
const getHVParameters = row => {
  const strippedRow = row.splice(6, row.length).map(parseFloat)
  return {
    Vscint0set: strippedRow[0],
    Vscint0read: strippedRow[1],
    Iscint0max: strippedRow[2],
    Iscint0read: strippedRow[3],
    scint0power: strippedRow[4],
    scint0status: strippedRow[5],
    Vscint1set: strippedRow[6],
    Vscint1read: strippedRow[7],
    Iscint1max: strippedRow[8],
    Iscint1read: strippedRow[9],
    scint1power: strippedRow[10],
    scint1status: strippedRow[11],
    Vrpc0set: strippedRow[18],
    Vrpc0read: strippedRow[19],
    Irpc0max: strippedRow[20],
    Irpc0read: strippedRow[21],
    rpc0power: strippedRow[22],
    rpc0status: strippedRow[23],
    Vrpc1set: strippedRow[24],
    Vrpc1read: strippedRow[25],
    Irpc1max: strippedRow[26],
    Irpc1read: strippedRow[27],
    rpc1power: strippedRow[28],
    rpc1status: strippedRow[29]
  }
}

module.exports = {
  getLastRow,
  parseDateTimeFromRow,
  getUpdatedFileDate,
  getHVParameters
}

/**
 * Testing the utilities
 */
// getLastRow().then(row => console.log(parseDateTimeFromRow(row)))

// getUpdatedFileDate("./data/VIMonitor_legenda.txt")
//   .then(date => console.log(date))
//   .catch(err => console.log(err))
const row = [
  "2018",
  "1",
  "16",
  "17",
  "2",
  "30",
  "1500",
  "1499",
  "335",
  "330",
  "1",
  "1",
  "1500",
  "1500.2",
  "335",
  "329.6",
  "1",
  "1",
  "1970",
  "0",
  "1500",
  "0",
  "0",
  "0",
  "9000",
  "8974",
  "0.5",
  "0.29",
  "1",
  "1",
  "10200",
  "10195",
  "0.3",
  "0.12",
  "1",
  "1"
]
