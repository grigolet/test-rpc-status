// Load the dependencies
const server = require("server")
const { get } = server.router
const { render } = server.reply
const utils = require("./utils")
const port = process.env.PORT || process.env.OPENSHIFT_NODEJS_PORT || 8080

// Load the userlist (if cache is expired) and render the index
const homeRoute = get("/", async () => {
  const data = await getSomeData()
  return render("index.hbs", { data })
})

// Launch the server in port 3000
server(port, homeRoute)

// Return some random data
const getSomeData = async () => {
  const lastUpdated = await utils.getUpdatedFileDate("./data/VIMonitor.out")
  const lastRow = await utils.getLastRow("./data/VIMonitor.out")
  const lastUpdatedFromRow = await utils.parseDateTimeFromRow(lastRow)
  const HVparameters = utils.getHVParameters(lastRow)
  return {
    lastUpdated,
    lastRow,
    lastUpdatedFromRow,
    HVparameters
  }
}
